﻿namespace ContosoUniversity.AuthServer.Infrastructure.Enums
{
    public enum Role
    {
        Admin,
        Instructor,
        Student
    }
}
