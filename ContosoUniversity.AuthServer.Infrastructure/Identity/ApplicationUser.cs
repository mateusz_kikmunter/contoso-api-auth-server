﻿using Microsoft.AspNetCore.Identity;

namespace ContosoUniversity.AuthServer.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Active { get; set; }
    }
}
