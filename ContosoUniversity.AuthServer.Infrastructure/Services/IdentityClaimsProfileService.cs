﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ContosoUniversity.AuthServer.Infrastructure.Enums;
using ContosoUniversity.AuthServer.Infrastructure.Identity;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.AuthServer.Infrastructure.Services
{
    public class IdentityClaimsProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory;
        private readonly ApplicationDbContext _applicationDbContext;

        public IdentityClaimsProfileService(UserManager<ApplicationUser> userManager, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, ApplicationDbContext applicationDbContext)
        {
            _userManager = userManager;
            _claimsFactory = claimsFactory;
            _applicationDbContext = applicationDbContext;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            var principal = await _claimsFactory.CreateAsync(user);

            var claims = principal.Claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();
            var userRole = await _applicationDbContext.UserClaims.FirstOrDefaultAsync(c => c.UserId.Equals(sub) && c.ClaimType.Equals("Role"));
            claims.Add(new Claim("role", userRole?.ClaimValue ?? Role.Student.ToString()));

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null && user.Active;
        }
    }
}
