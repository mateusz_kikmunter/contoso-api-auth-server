﻿using ContosoUniversity.AuthServer.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.AuthServer.Infrastructure.Helpers.DataAccessHelpers
{
    public class ApplicationDbContextFactory : DesignTimeDbContextFactoryBase<ApplicationDbContext>
    {
        protected override ApplicationDbContext CreateNewInstance(DbContextOptions<ApplicationDbContext> options)
        {
            return new ApplicationDbContext(options);
        }
    }
}
