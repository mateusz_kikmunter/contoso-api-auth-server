﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ContosoUniversity.AuthServer.Infrastructure.Enums;
using ContosoUniversity.AuthServer.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace ContosoUniversity.AuthServer.Infrastructure.Helpers
{
    public static class Seed
    {
        public static async Task SeedUsersAsync(IServiceProvider serviceProvider)
        {
            using (var applicationDbContext = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                if (!applicationDbContext.Users.Any())
                {
                    var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                    var admin = new ApplicationUser
                    {
                        FirstName = "Admin",
                        LastName = "Almighty",
                        Active = true,
                        EmailConfirmed = true,
                        Email = "admin.contoso@edu.com",
                        UserName = "admin.contoso@edu.com"
                    };

                    var claims = new List<Claim>
                    {
                        new Claim("Role", Role.Admin.ToString()),
                        new Claim("FirstName", admin.FirstName),
                        new Claim("LastName", admin.LastName),
                        new Claim("Email", admin.Email),
                        new Claim("Active", admin.Active.ToString())
                    };

                    await userManager.CreateAsync(admin, "P@ssword1");
                    await userManager.AddClaimsAsync(admin, claims);
                }
            }
        }
    }
}
