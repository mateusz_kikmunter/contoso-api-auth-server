# About
Old EF Core and MVC tutorial rewritten to .NET Core WebApi + JWT authentication.

this is Identity provider part.

Web Api part - https://bitbucket.org/mateusz_kikmunter/contoso-api/src/master/

Angular Spa part - https://bitbucket.org/mateusz_kikmunter/contoso-client/src/master/
