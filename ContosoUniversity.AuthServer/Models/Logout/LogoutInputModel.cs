﻿namespace ContosoUniversity.AuthServer.Models.Logout
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
