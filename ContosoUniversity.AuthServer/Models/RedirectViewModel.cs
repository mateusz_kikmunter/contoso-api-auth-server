﻿

namespace ContosoUniversity.AuthServer.Models
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}
