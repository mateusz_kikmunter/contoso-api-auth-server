﻿using ContosoUniversity.AuthServer.Infrastructure.Identity;

namespace ContosoUniversity.AuthServer.Models.Register
{
    public class RegisterResponseViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public RegisterResponseViewModel(ApplicationUser user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Active = user.Active;
        }
    }
}
