﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace ContosoUniversity.AuthServer.Configuration.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile()
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("university-api", "University API")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new[]
            {
                new Client {
                    RequireConsent = false,
                    ClientId = "university_spa",
                    ClientName = "Contoso University SPA",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes = { "openid", "profile", "email", "university-api" },
                    RedirectUris = new List<string>
                    {
                        "https://localhost:4200/login",
                        "https://localhost:4200/silent-renew"
                    },
                    PostLogoutRedirectUris = {"https://localhost:4200/welcome"},
                    AllowedCorsOrigins = {"https://localhost:4200"},
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenLifetime = 3600,
                    IdentityTokenLifetime = 3600
                }
            };
        }
    }
}
